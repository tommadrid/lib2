<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-container">
	<div class="main-grid">
		<main class="main-content post-page">

		<h1><?php the_title() ?></h1>

		<div class="post-image-wrapper">
			<div class="mobile-post-image"><img src="<?php the_post_thumbnail_url( 'large' ); ?>"></div>
			<div class="post-image" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')"></div>
		</div>
		<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="entry-content">
			<?php the_content(); ?>
			</div>
			<?php // the_post_navigation(); ?>
				<?php // comments_template(); ?>
			<?php endwhile; ?>
		</main>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();
