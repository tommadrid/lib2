<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

$link_embed = get_post_meta(get_the_ID(), 'link_embed');

if($link_embed[0] != "") :
	$link = $link_embed[0];
else :
	$link = get_permalink($post->ID);
endif;

?>
<article class="news-feed_item" data-src="<?=the_post_thumbnail_url( 'full' )?>">
	<a  href="<?= $link; ?>"></a>
	<div class="copy-container">
		<!-- <span class="post-card-author"><?php // the_author(); ?></span> -->
		<h2 class="post-card-title"><?php  the_title(); ?></h2>
		<p><?php the_excerpt(); ?></p>
		<a class="post-card-read-more" href="<?= $link ?>">Read More</a>
	</div>
</article>