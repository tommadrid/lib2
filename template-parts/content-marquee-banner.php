<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="marquee-item">
	<h6 class="title"><a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a></h6>
</div>
			