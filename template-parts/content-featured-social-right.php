<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

	<artical class="row featured-social_item">

		<div class="large-6  medium-6 columns remove-padding-right mobile">
			<div class="featured-image" style="background-image: url('<?=the_post_thumbnail_url( 'full' )?>')" data-src="<?=the_post_thumbnail_url( 'full' )?>"></div>
		</div>

		<div class="large-6 medium-6 columns content-section">
			<div class="text-wrapper">
				<h3>
					<?php the_title(); ?>
				</h3>
				<div class="author-wrapper">
					<span class="by">by </span>
					<span class="artical-author">
						<?php the_author(); ?>
					</span>
				</div>
				<?php $content = get_the_content();
				$trimmed_content = wp_trim_words( $content, 100, '...' ); ?>
				<p><?php echo $trimmed_content; ?></p>
			</div>
			<div class="read-more-block-wrapper">
				<div class="row read-more-container">

					<div class="large-6 small-6 columns">
					<?php 
						$link_embed = get_post_meta(get_the_ID(), 'link_embed');

						if($link_embed[0] != "") :
							$link = $link_embed[0];
						else :
							$link = get_permalink($post->ID);
						endif;
					?>
					<a href="<?= $link ?>">Read More</a>

					</div>
					<div id="gallery-arrows" class="large-6 small-6 columns arrow-wrapper">
						<div class="prevArrow">
							<span></span>
						</div>
						<span class="count"><?= $currentCount . ' / ' . $totalNumber ?></span>
						<div class="nextArrow">
							<span></span>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="large-6  medium-6 columns remove-padding-right desktop">
			<div class="featured-image" style="background-image: url('<?=the_post_thumbnail_url( 'full' )?>')" data-src="<?=the_post_thumbnail_url( 'full' )?>"></div>
		</div>
	</artical>
