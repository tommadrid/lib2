<?php
/**
 * Template part for mobile top bar menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-menu vertical menu" id="<?php foundationpress_mobile_menu_id(); ?>" role="navigation">
	
	<?php foundationpress_mobile_nav(); ?>

	<div class="top-bar-right">

	<a target="_blank" href="https://www.instagram.com/lifeisbeautiful">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/instagram-icon.svg">
						</a>
						<a target="_blank" href="https://www.facebook.com/LifeIsBeautifulFest/">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/facebook.svg">
						</a>
						<a target="_blank" href="https://twitter.com/lifeisbeautiful/">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/twitter.svg">
						</a>
						<a href="mailto:LIBPress@rrpartners.com?subject=Life%20Is%20Beautiful%20General%20Inquiry">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/mail.svg">
						</a>
						<a target="_blank" href="https://www.youtube.com/user/LifeIsBeauitful/videos">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/youtube.svg">
						</a>
	</div>

</nav>
