<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

	<article class="related-post">
		<div class="copy-container">
			<h5 class="title"><a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a></h5>
			<span>By <?php the_author() ?></span>
			<p><?= wp_trim_words(get_the_content(), 25); ?></p>
		</div>
	</article>
