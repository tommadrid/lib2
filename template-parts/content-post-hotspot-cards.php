<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
	<article class="news-feed_item hyperlinks" data-src="<?=the_post_thumbnail_url( 'full' )?>">
		<a href="#<?= get_post_field( 'post_name', get_post() ); ?>">
			<div class="copy-container">
				<h2 class="title">
					<?php the_title(); ?>
				</h2>
				<p><?php the_content(); ?></p>
				<a class="post-card-read-more" href="#<?= get_post_field( 'post_name', get_post() ); ?>">Discover More</a>
			</div>
		</a>
	</article>
