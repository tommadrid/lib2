<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );

/**
* Add The Meta Box
**/


function add_embed_link_meta_box() {
    add_meta_box(
        'paulund_embed_tweet_meta_box', // $id
        'Post Alternate Link', // $title
        'show_embed_link_meta_box', // $callback
        'post', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'add_embed_link_meta_box');

/**************************************************************************
 * Save the meta fields on save of the post
 **************************************************************************/
function save_embed_tweet_meta($post_id) {   
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }  
    
    $old = get_post_meta($post_id, "link_embed", true);
    
    $new = $_POST["link_embed"];
    
    if ($new && $new != $old) {
        update_post_meta($post_id, "link_embed", $new);
    } elseif ('' == $new && $old) {
        delete_post_meta($post_id, "link_embed", $old);
    }
}
add_action('save_post', 'save_embed_tweet_meta');

function show_embed_link_meta_box() {
    global $post;  
        $meta = get_post_meta($post->ID, 'link_embed', true);  
	
    // Use nonce for verification  
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
   
   echo '<table class="form-table">';           
        echo '<tr>
            <th><label for="link_embed">External Link</label></th>
            <td><input name="link_embed" id="link_embed" value="'.$meta.'">
		        <span class="description">Use to replace Read More link.  Will overwrite Read More link on this post preventing linking to post details page and linking to the external reference entered here.</span></td>
            </tr>';
            
    echo '</table>';
}