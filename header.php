<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
	<!doctype html>

	<html class="no-js" <?php language_attributes(); ?> >

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>

		<!-- Google Tag Manager -->
		<script>
			(function (w, d, s, l, i) {
				w[l] = w[l] || [];
				w[l].push({'gtm.start': new Date().getTime(),
					event: 'gtm.js'
				});
				var f = d.getElementsByTagName(s)[0],
					j = d.createElement(s),
					dl = l != 'dataLayer' ? ' & l = '+l : '';
				j.async = true;
				j.src = 'https: //www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window, document, 'script', 'dataLayer', 'GTM-NQ9K55G');

		</script>
		<!-- End Google Tag Manager -->
	</head>

	<body class="disable-scroll">

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQ9K55G" height="0" width=“0” style="display:none;visibility:hidden"></iframe></noscript>

		<div id="page-loader">
			<div class="loader-container">
				<svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				  viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
					<path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50"></path>
				</svg>
			</div>
		</div>


		<?php // get_template_part( 'template-parts/mobile-off-canvas' ); ?>

		<header class="site-header" role="banner">

			<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
				<div class="top-bar-left">
					<img class="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/logo.svg" alt="">
				</div>
				<div class="title-bar-right">
					<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
					<span class="site-mobile-title title-bar-title">
					</span>
				</div>
			</div>

			<nav class="site-navigation top-bar title-bar" role="navigation">
				<div class="top-bar-left">
					<div class="site-desktop-title top-bar-title main-nav">
						<img class="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/logo.svg" alt="">
						<?php echo foundationpress_main_nav(); ?>
					</div>
				</div>
				<div class="top-bar-right">
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
					<div class="desktop-icons">
						<a target="_blank" href="https://www.instagram.com/lifeisbeautiful">
							<i class="fab fa-instagram"></i>
						</a>
						<a target="_blank" href="https://www.facebook.com/LifeIsBeautifulFest/">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a target="_blank" href="https://twitter.com/lifeisbeautiful/">
							<i class="fab fa-twitter"></i>
						</a>
						<!-- <a href="mailto:press@lifeisbeautiful.com?subject=Life%20Is%20Beautiful%20General%20Inquiry">
							<i class="far fa-envelope"></i>
						</a> -->
						<a target="_blank" href="https://www.youtube.com/user/LifeIsBeauitful/videos">
							<i class="fab fa-youtube"></i>
						</a>

						<!-- <form role="search" method="get" id="searchform" action="http://lib.local:8888/">
							<div class="input-group">
								<input type="text" class="input-group-field" value="" name="s" id="s" placeholder="Search">
								<div class="input-group-button">
								<img src="<?php // echo get_stylesheet_directory_uri(); ?>/dist/assets/images/icons/search.svg">
								</div>
							</div>
						</form> -->

					</div>
				</div>
				</div>
			</nav>

		</header>
