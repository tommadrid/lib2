$(function () {

	$('.lazy').lazy({
		effect: 'fadeIn',
		effectTime: 500,
		removeAttribute: false,
		afterLoad: function(element){
			element.addClass('loaded');
		}
	});
	$('.lazyVideoBlock').lazy({
		effect: 'fadeIn',
		effectTime: 500,
		// removeAttribute: false,
		afterLoad: function(element){
			element.addClass('loaded');
			element.fancybox({
				buttons : [
					'close'
				],
				margin: 0
			})
		}
	});
	$('.input-group-button').on('click', function(){
		var searchInput = $('.input-group-field');
		searchInput.toggleClass('show');
		searchInput.focus();

	});
	$('[data-fancybox] a').on('click', function(e){
		e.preventDefault();
		var win = window.open($(this).attr('href'), '_blank');
  		win.focus();
	})
	$("[data-fancybox]").fancybox({
		buttons : [
			'close'
		],
		margin: 0
	});


	$(".artist-modal").fancybox({
		buttons : [
			'close'
		],
		margin: 0,
		beforeShow: function (elm, current) {

			var selectedArtist = current.opts.$orig.data('artist-id');

			var data = doStuff.getModelData(selectedArtist);

			console.log(data);

			artistHtml = '<div id="artist-image" class="artist-image" style="background-image: url('+data.festival_artist.image+')"></div>';
			artistHtml += '<div class="artist-content-wrapper">';

			artistHtml += '<div class="artist-title-wrapper">';

			artistHtml += '<div class="artist-social">';
			artistHtml += '<a target="_blank" href="'+data.festival_artist.facebook_url+'"><i class="fi-social-facebook"></i></a>';
			artistHtml += '<a target="_blank" href="'+data.festival_artist.instagram_url+'"><i class="fi-social-instagram"></i></a>';
			artistHtml += '<a target="_blank" href="'+data.festival_artist.twitter_url+'"><i class="fi-social-twitter"></i></a>';
			artistHtml += '<a target="_blank" href="'+data.festival_artist.youtube_channel_url+'"><i class="fi-social-youtube"></i></a>';
			artistHtml += '</div>'

			artistHtml += '<div id="artist-name" class="artist-name"><h4>'+data.festival_artist.name+'</h4></div>';
			artistHtml += '<div id="artist-name" class="artist-website"><a target="_blank" href="'+data.festival_artist.web_site_url+'">Visit our website</a></div>';

			artistHtml += '</div>';

			artistHtml += '<div class="artist-music-block">'+data.festival_artist.music_embed+'</div>';

			artistHtml += '<div class="artist-video-block">'+data.festival_artist.video_embed+'</div>';

			artistHtml += '<div class="artist-bio-block">'+data.festival_artist.bio+'</div>';

			artistHtml += '</div>';

			$("#artist-content").html(artistHtml);


		}
	});


	$('.gallery-logo-module').slick({
		infinite: true,
		speed: 250,
		slidesToShow: 4,
		centerMode: false,
		variableWidth: false,
		easing: 'easeOutExpo',
		lazyLoad: 'ondemand',
		autoplay: true,
		arrows: false,
		pauseOnHover: false,
		prevArrow: '',
		nextArrow: '<div class="galleryNextArrow"><span></span></div>',
		responsive: [{
			breakpoint: 800,
			settings: {
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	  });

	$('.gallery-module').on('init', function(){
		$('figure').Lazy({
			effect: 'fadeIn',
			effectTime: 500,
			removeAttribute: false
		});
	});
	$('.gallery-module').on('beforeChange', function () {
		$(this).find('figure').each(function () {
			if ($(this).css('background-image') == 'none') {
				$(this).css('background-image', 'url(' + $(this).data('src') + ')');
			}
		});
	});
	$('.gallery-module.ltr').slick({
		infinite: true,
		speed: 250,
		centerMode: false,
		variableWidth: true,
		easing: 'easeOutExpo',
		lazyLoad: 'ondemand',
		prevArrow: '',
		nextArrow: '<div class="galleryNextArrow"><span></span></div>',
	});
	$('.gallery-module.rtl').slick({
		infinite: true,
		speed: 250,
		centerMode: false,
		variableWidth: true,
		easing: 'easeOutExpo',
		lazyLoad: 'ondemand',
		rtl: true,
		prevArrow: '',
		nextArrow: '<div class="galleryPrevArrow"><span></span></div>',
	});



	$('.featured-artists').on('init', function(){
		$('.featured-artist_item').Lazy({
			effect: 'fadeIn',
			effectTime: 500,
			removeAttribute: false
		});
	});
	$('.featured-artists').on('breakpoint', function(){
		$('.featured-artist_item').each(function(){
			$(this).css({
				'background-image': 'url(' + $(this).data('src') + ')'
			})
		});
	});
	$('.featured-artists').slick({
		infinite: false,
		speed: 250,
		dots: false,
		centerMode: false,
		slidesToShow: 4,
		prevArrow: '<div class="gPrevArrow"><span></span></div>',
		nextArrow: '<div class="gNextArrow"><span></span></div>',
		responsive: [{
			breakpoint: 800,
			settings: {
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	})
	$('.news-feed').on('init', function(){
		$('.news-feed_item').Lazy({
			effect: 'fadeIn',
			effectTime: 500,
			removeAttribute: false
		});
	});
	$('.news-feed').on('breakpoint', function(){
		$('.news-feed_item').each(function(){
			$(this).css({
				'background-image': 'url(' + $(this).data('src') + ')'
			})
		});
	});
	$('.news-feed').slick({
		infinite: false,
		speed: 250,
		centerMode: false,
		lazyLoad: 'ondemand',
		slidesToShow: 4,
		prevArrow: '<div class="gPrevArrow"><span></span></div>',
		nextArrow: '<div class="gNextArrow"><span></span></div>',
		responsive: [{
			breakpoint: 800,
			settings: {
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});



  var slider = $('.featured-social');

  slider.slick({
    infinite: false,
		dots: false,
    speed: 250,
    centerMode: false,
    prevArrow: '',
    nextArrow: '',
    slidesToShow: 1,
    responsive: [{
      breakpoint: 800,
      settings: {
				dots: true,
        slidesToShow: 1,
        // slidesToScroll: 1
      }
    }]
  });

  $('.prevArrow').on('click', function () {
    slider.slick('slickPrev');
  });

  $('.nextArrow').on('click', function () {
    slider.slick('slickNext');
  });

  $('.featured-social').on('beforeChange', function () {
    $(this).find('img').each(function () {
      if (typeof $(this).attr('src') == 'undefined') {
        $(this).attr('src', $(this).data('src'));
      }
    })

  });

  $(".hyperlinks").find("a").on('click', function (e) {
    console.log('test click');
    e.preventDefault();
    var section = $(this).attr("href");
    $("html, body").animate({
      scrollTop: $(section).offset().top - 120
    });
  });
	$('#page-loader').fadeOut(250, function () {
    $('body').removeClass('disable-scroll');
    $(this).remove();
  });
})
